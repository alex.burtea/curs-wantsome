#!/usr/bin/env python3

def main():
    """ Scrieți un program Python care să determine frecvența unei litere primite de la tastatură într-un șir de caractere."""

    text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    litera = input('Introduceti o litera: ')
    if not litera.isalpha() or len(litera) > 1:
        print('Serios? %s ti se pare a fi litera?' % litera)
    else:
        print('Super ne apucam de treaba.')
        print('Numarul de aparitii pentru %r este: %d' %
              (litera, text.count(litera)))
        numar_aparitii = 0
        for caracter in text:
            # if caracter.lower() == litera.lower():
            if caracter == litera:
                numar_aparitii = numar_aparitii + 1
        print('Numarul de aparitii pentru %r este: %d' %
              (litera, numar_aparitii))

        minim = len(text) + 1
        maxim = -1
        litera_minim = ''
        litera_maxim = ''

        litere_valide = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        # for pozitie_in_alfabet in range(ord('a'), ord('z') + 1):
        #    caracter = chr(pozitie_in_alfabet)
        for caracter in litere_valide:
            aparitii = 0
            for element in text:
                if element == caracter:
                    aparitii = aparitii + 1
            if aparitii > maxim:
                maxim = aparitii
                litera_maxim = caracter
            if aparitii < minim:
                minim = aparitii
                litera_minim = caracter

        print(litera_minim, minim)
        print(litera_maxim, maxim)


if __name__ == "__main__":
    main()
