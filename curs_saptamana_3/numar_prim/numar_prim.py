#!/usr/bin/env python3

"""Scrieți un program Python care să verifice dacă un număr primit de la tastatură este prim. Se consideră 
prim un număr mai mare ca 1 dacă acesta are doar doi divizori, 1 și el însuși."""


def main():
    is_prime = input('introduceti un numar: ')
    pozitia = 0
    numar_valid = True

    for caracter in is_prime:
        pozitia += 1
        if pozitia == 1:
            if not(caracter in '-+|' or caracter.isdigit()):
                numar_valid = False
            else:
                if not caracter.isdigit():
                    numar_valid = False

    if numar_valid:
        is_prime = int(is_prime)
    else:
        print('numarul introdus nu este un numar')

    if is_prime == 0 or is_prime > 1:
        print(is_prime, 'nu este numar prim!')
    for item in range(2, int(is_prime)):
        if int(is_prime) % item == 0:
            print(is_prime, 'nu este numar prim!')
    else:
        print(is_prime, 'este un numar prim!')


if __name__ == "__main__":
    main()
