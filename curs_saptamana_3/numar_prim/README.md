Scrieți un program Python care să verifice dacă un număr primit de la tastatură este prim. 
Se consideră prim un număr mai mare ca 1 dacă acesta are doar doi divizori, 1 și el însuși.

Suplimentar:

propuneți o soluție alternativă cu fiecare dintre restricțiile de mai jos:
nu va fi folosită bucla for
nu va fi folosită bucla while
nu va fi folosită funcția range