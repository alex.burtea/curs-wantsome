#!/usr/bin/env python3

def main():


""" Scrieți un program Python care să verifice dacă un număr primit de la tastatură este palindrom. Un număr este considerat palindrom dacă numărul citit de la dreapta la stânga este egal cu numărul citit de la stânga la dreapta. Câteva exemple de numere palindrom: 121, 12321, 55555.

Cerințe:

numărul va trebui validat
nu va fi tratat ca un șir de caractere."""

is_palindrome = input('introduc eti un numar format din minim 2 cifre : ')

pozitia = 0
numar_valid = True

for caracter in is_palindrome:
    pozitia += 1
    if pozitia == 1:
        if not(caracter in '-+|' or caracter.isdigit()):
            numar_valid = False
        else:
            if not caracter.isdigit():
                numar_valid = False

if numar_valid:
    is_palindrome == int(is_palindrome)

else:
    print('numarul introdus nu este un numar')

while is_palindrome == is_palindrome[::-1]:
    print(is_palindrome, ' este un numar palindrom!')
    break
else:
    print(is_palindrome, ' nu este un numar palindrom!')

if __name__ == "__main__":
    main()
