#!/usr/bin/env python3


def main():
    '''Python program for interchange of two values'''
    def interchange():
        first_value = input('enter first caracter: ')
        second_value = input('enter 2nd caracter: ')
        third_value, first_value = first_value, second_value
        first_value = third_value
        print('The interchange of those two values entered is:  ', first_value,
              second_value)

    interchange()


if __name__ == "__main__":
    main()
