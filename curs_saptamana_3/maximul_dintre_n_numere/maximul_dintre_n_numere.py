#!/usr/bin/env python3

def main():
    """ Scrieți un program Python care să calculeze maximul dintre două valori numerice primite de la tastatură.

Cerințe:

nu se va folosi funcția max
valorile primite vor fi validate înainte de a fi folosite în comparație
în cazul în care valorile nu sunt potrivite programul va afișa un mesaj de eroare
Suplimentar:

extindeți programul precendent pentru a putea calcula maximul dintre n numere primite de la tastatură
prima dată se va citi numărul de numere acceptate
după care se va citi fiecare dintre aceste numere
fiecare număr va fi validat
la final se va afișa maximul."""

    index = 1
    maximul = 0

    while True:
        numar_maxim_de_numere = input('Introduceti numarul maxim de numere: ')
        if numar_maxim_de_numere.isnumeric():
            numar_maxim_de_numere = int(numar_maxim_de_numere)
            break
        print('Numarul maxim de numere nu este un numar valid')
        print('Mai incearca')

    while index < numar_maxim_de_numere + 1:
        numar = input('Introduceti numarul %d: ' % index)
        este_valid = True
        for pozitie, caracter in enumerate(numar):
            if pozitie == 0:
                if not (caracter.isdigit() or caracter in '-+'):
                    este_valid = False
            else:
                if not caracter.isdigit():
                    este_valid = False

        if not este_valid:
            print('Numarul nu este valid.')
            print('Incearca din nou')
        else:
            numar = int(numar)
            if index == 1:
                maximul = numar
            else:
                if maximul < numar:
                    maximul = numar
            index = index + 1

    print('Maximul pentru cele %d numere este: %s' % (index - 1, maximul))


if __name__ == "__main__":
    main()
